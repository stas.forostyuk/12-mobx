export { Landing } from './Landing/Landing'
export { RegistrationPage } from './RegistrationPage/RegistrationPage';
export { SignInPage } from './SignInPage/SignInPage';
export { RoomSearchPage } from './RoomSearchPage/RoomSearchPage';
export { RoomDetailsPage } from './RoomDetailsPage/RoomDetailsPage';
export { UserPage } from './UserPage/UserPage';
