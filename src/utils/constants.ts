const defaultRoom = {
  accessibility: { wideCorridor: true, assistant: false },
  additionalFacilities: {
    breakfast: true,
    crib: false,
    feedingChair: false,
    shampoo: false,
    tv: false,
    writingDesk: false,
  },
  freeDays: {
    from: '2022-10-11T06:17:02.467Z',
    to: '2022-10-18T00:20:15.895Z',
  },
  guests: { kids: 2, baby: 0, adults: 4 },
  id: 10,
  imgSrc: [
    'https://firebasestorage.googleapis.com/v0/b/toxin-…=media&token=43bbc6bb-a9a8-4731-ba0e-89784af2195e',
    'https://firebasestorage.googleapis.com/v0/b/toxin-…=media&token=43bbc6bb-a9a8-4731-ba0e-89784af2195e',
    'https://firebasestorage.googleapis.com/v0/b/toxin-…=media&token=43bbc6bb-a9a8-4731-ba0e-89784af2195e',
    'https://firebasestorage.googleapis.com/v0/b/toxin-…=media&token=43bbc6bb-a9a8-4731-ba0e-89784af2195e',
  ],
  number: 156,
  price: 11423,
  rating: 5,
  reviewsAmount: 99,
  roomAmenities: { bedrooms: 2, beds: 4, bathrooms: 0 },
  rules: { allowSmoke: false, allowGuests: true, allowPets: false },
  type: 'Люкс',
};

export { defaultRoom };
