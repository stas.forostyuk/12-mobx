const links = [
  {
    title: 'навигация',
    links: [
      { text: 'О нас', link: '/mock-address' },
      { text: 'Новости', link: '/mock-address' },
      { text: 'Служба поддержки', link: '/mock-address' },
      { text: 'Услуги', link: '/mock-address' },
    ],
  },
  {
    title: 'о нас',
    links: [
      { text: 'О сервисе', link: '/mock-address' },
      { text: 'Наша команда', link: '/mock-address' },
      { text: 'Вакансии', link: '/mock-address' },
      { text: 'Инвесторы', link: '/mock-address' },
    ],
  },
  {
    title: 'Служба поддержки',
    links: [
      { text: 'Соглашения', link: '/mock-address' },
      { text: 'Сообщества', link: '/mock-address' },
      { text: 'Связь с нами', link: '/mock-address' },
    ],
  },
];

export { links };