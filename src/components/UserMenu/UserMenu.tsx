import { FC } from 'react';

import { Authentication } from './Authentication/Authentication';
import { ProfileLink } from './ProfileLink/ProfileLink';

type Props = {
  profileName?: string;
};

const UserMenu: FC<Props> = ({ profileName = '' }) => {
  return (
      profileName.trim() ? <ProfileLink name={profileName} /> : <Authentication />
  );
};

export { UserMenu };
