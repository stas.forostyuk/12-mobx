import { Button } from 'components/Button/Button';

import styles from './Authentication.module.scss';

const Authentication = () => {
  return (
    <div className={styles.authentication}>
      <div className={styles.authentication__signIn}>
        <Button type="signIn" href='/sign-in'>
          Войти
        </Button>
      </div>
      <div className={styles.authentication__registration}>
        <Button type="registration" href="/register">
          Зарегистрироваться
        </Button>
      </div>
    </div>
  );
};

export { Authentication };
