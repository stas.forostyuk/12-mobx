import { FC } from 'react';
import { Link } from 'react-router-dom';
import { MdArrowForward } from 'react-icons/md';
import classNames from 'classnames/bind';

import styles from './Button.module.scss';

type Props = {
  type: 'signIn' | 'registration' | 'submit' | 'default';
  children: string;
  href?: string;
  disabled?: boolean;
  theme?: 'main' | 'landing' | 'details' | 'authorization';
  sizeFitContent?: boolean;
  onClick?: () => void;
};

const cn = classNames.bind(styles);

const Button: FC<Props> = ({
  type,
  children,
  href = 'some-link.html',
  disabled = false,
  theme = 'main',
  sizeFitContent = false,
  onClick,
}) => {
  const buttonClass = cn({
    button: true,
    button_type_signIn: type === 'signIn',
    button_type_registration: type === 'registration',
    button_type_submit: type === 'submit',
    button_in_landing: theme === 'landing',
    button_in_details: theme === 'details',
    button_in_authorization: theme === 'authorization',
    button_size_fitContent: sizeFitContent,
  });
  return type === 'submit' ? (
    <button type="submit" className={buttonClass} disabled={disabled}>
      <span className={styles.button__icon}>
        <MdArrowForward size="24px" />
      </span>
      {children}
    </button>
  ) : (
    <Link to={href || ''} className={buttonClass} onClick={onClick}>
      <span className={styles.button_type_signInText}>{children}</span>
    </Link>
  );
};

export { Button };
